import { takeEvery, call, put, all } from "redux-saga/effects"

export function* watchFetchProducts() {
  yield takeEvery("FETCH_PRODUCTS_START", fetchProducts)
}

export function* fetchProducts() {
  try {
    const response = yield call(
        fetch,
        'http://light-it-04.tk/api/posters/?format=json'
      );
    const parsedResult = yield call([response, response.json]);
    yield put({ type: "FETCH_PRODUCTS", payload: parsedResult.data})
  } catch (error) {
    yield put({ type: "FETCH_PRODUCTS", payload: []})
  }
}

export default function* rootSaga() {
  yield all([
    watchFetchProducts(),
  ])
}
