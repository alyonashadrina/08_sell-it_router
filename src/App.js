import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { composeWithDevTools } from "redux-devtools-extension"
import reduxThunk from "redux-thunk";
import { applyMiddleware, createStore } from "redux";
import reducer from "./reducers";
import { Provider } from 'react-redux';
import logger from "./middlewars/logger"
import createSagaMiddleware from "redux-saga"
import rootSaga from "./saga/products"
import './App.css';

import Footer from './components/Footer';
import Header from './components/Header';
import MainContent from './components/MainContent';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, composeWithDevTools(applyMiddleware(reduxThunk, logger, sagaMiddleware)))
sagaMiddleware.run(rootSaga);

const App = () => (
  <Provider store={store}>
    <Router>
      <div className="App">
        <Header />
        <MainContent/>
        <Footer />
      </div>
    </Router>
  </Provider>
)

export default App;
