import { LOG_IN } from '../types/account';

const logIn = () => dispatch => {
  dispatch({
      type: LOG_IN,
      payload: true
  })
}

const logOut = () => dispatch => {
  dispatch({
      type: LOG_IN,
      payload: false
  })
}

export { logIn, logOut }
