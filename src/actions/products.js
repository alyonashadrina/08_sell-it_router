import { FETCH_PRODUCTS, FETCH_SINGLE_PRODUCT } from '../types/products';

// const fetchProducts = () => dispatch => {
//     fetch(
//         "http://light-it-04.tk/api/posters/?format=json"
//     )
//         .then(res => res.json())
//         .then(
//             result => {
//                 dispatch({
//                     type: FETCH_PRODUCTS,
//                     payload: result.data,
//                     meta: {
//                         printLog: true
//                     }
//                 })
//               // console.log('fetch', result.data)
//             },
//
//             error => {
//                 console.log("error", this);
//             }
//         );
// }

const fetchProducts = () => {
  return { type: "FETCH_PRODUCTS_START" }
}

const fetchSingleProduct = (id) => dispatch => {
  fetch(
      `http://light-it-04.tk/api/posters/${id}?format=json`
  )
      .then(res => res.json())
      .then(
          result => {
              dispatch({
                  type: FETCH_SINGLE_PRODUCT,
                  payload: result
              })
            console.log('fetch product', result)
          },

          error => {
              console.log("error", this);
          }
      );
}

export { fetchProducts, fetchSingleProduct }
