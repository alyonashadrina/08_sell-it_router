import { LOG_IN } from '../types/account';

const initialState = {
  loggedIn: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN:
        return { ...state, loggedIn: action.payload }
    default:
      return state;
  }
};
