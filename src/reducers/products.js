import { FETCH_PRODUCTS, FETCH_SINGLE_PRODUCT } from '../types/products';

const initialState = {
  productList: [],
  singleProduct: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS:
        return { ...state, productList: action.payload }
    case FETCH_SINGLE_PRODUCT:
      return { ...state, singleProduct: action.payload }
    default:
      return state;
  }
};
