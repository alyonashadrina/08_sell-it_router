import React from 'react';
import styled, { css } from 'styled-components';
import { styleVars } from '../styleVars';
import { Wrapper, LabelButton } from '../styleClasses';

import logoWhiteMd from '../assets/logo-white--md.png';
import FormSignIn  from './FormSignIn';
import FormSignUp  from './FormSignUp';

import { connect } from "react-redux";
import { modeToLogin, modeToRegister } from '../actions/layout';

const mapStateToProps = state => ({ logMode: state.layout.logMode });
const mapDispatcheToProps = dispatch => ({ modeToRegister: () => dispatch(modeToRegister()), modeToLogin: () => dispatch(modeToLogin()) });

const Dropdown = styled(Wrapper)`
  max-width: 100%;
  position: fixed;
  width: ${styleVars.other.sidePanelWidth};
  height: 100vh;
  right: 0;
  background: ${styleVars.colors.primary};
  color:${styleVars.colors.onPrimaryBg};
  text-align: center;
  padding-bottom: ${styleVars.spaces.spaceSm};
  padding-top: calc((${styleVars.spaces.spaceSm} * 2 + ${styleVars.fontSizes.base} * ${styleVars.typography.lineHeight}) * 2);
  top: 0;
  transition: transform .2s;
  transform: translateY(-100%);
  ${props => props.opened && css`
    transform: translateY(0);
  `}
`;

const LogMode = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: ${styleVars.spaces.spaceSm};
  padding-bottom: 5vh;
  padding-top: 3vh;

`;

const ModeButton = styled(LabelButton)`
  position: relative;
  text-transform: capitalize;
  background: white;
  color: ${styleVars.colors.text};
  input {
    position: absolute;
    opacity: 0;
  }
  ${props => props.active && css`
    box-shadow: inset  0px -2px 0px 1px ${styleVars.colors.selected};
  `}
`;
let submit = values => {
     console.log (values)
   }

const HeaderDrawer = (props) => (
  <Dropdown opened={props.opened}>
    <img className="side-panel__logo" src={logoWhiteMd} alt="Sell it! - logo"/>
    <LogMode>
      <ModeButton active={ props.logMode === 'login' ? true : false } onClick={props.modeToLogin}>
        <input type="radio" name="mode" value="Sign In" />Sign In
      </ModeButton>
      <ModeButton active={ props.logMode === 'login' ? false : true } onClick={props.modeToRegister}>
        <input type="radio" name="mode" value="Sign Up"/>Sign Up
      </ModeButton>
    </LogMode>
    <div className="side-panel__form">
    { props.logMode === 'login' ? <FormSignIn onSubmit={submit}/> : <FormSignUp /> }
    </div>
  </Dropdown>
)
export default connect(mapStateToProps, mapDispatcheToProps)(HeaderDrawer)
