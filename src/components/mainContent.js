import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import ProductList from '../pages/ProductList';
import Login from '../pages/Login';
import ProductPage from '../pages/ProductPage';

import { connect } from "react-redux";
import { closeDrawer } from '../actions/layout';

const mapStateToProps = state => ({ drawerOpened: state.layout.drawerOpened });
const mapDispatcheToProps = dispatch => ({
  closeDrawer: () => dispatch(closeDrawer()),
 });

const MainContent = (props) => (
  <div onClick={props.closeDrawer}>
    <Route
      exact path="/"
      render={props => <ProductList {...props} />}
    />
    <Route
      path="/login"
      render={props => <Login {...props} />}
    />
   <Route path="/products/:productId"
      render={({ match }) => (
        <ProductPage id={match.params.productId} />
      )} />
  </div>
)

export default connect(mapStateToProps, mapDispatcheToProps)(MainContent)
