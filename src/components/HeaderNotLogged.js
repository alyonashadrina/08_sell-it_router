import React, { Component } from 'react';
import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { Wrapper } from '../styleClasses';

import iconAccount from '../assets/icon-account.svg';
import HeaderDrawer from './HeaderDrawer';

import { connect } from "react-redux";
import { modeToLogin, modeToRegister, openDrawer, closeDrawer } from '../actions/layout';

const mapStateToProps = state => ({ logMode: state.layout.logMode, drawerOpened: state.layout.drawerOpened });
const mapDispatcheToProps = dispatch => ({
  modeToRegister: () => dispatch(modeToRegister()),
  modeToLogin: () => dispatch(modeToLogin()),
  openDrawer: () => dispatch(openDrawer()),
  closeDrawer: () => dispatch(closeDrawer()),
 });


const AccountNotLogged = styled.div`
  padding-top: ${styleVars.spaces.spaceXsm};
  padding-bottom: ${styleVars.spaces.spaceXsm};
  background-color: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  position: relative;
  display: grid;
  align-items: center;
  position: relative;
  a {
    color: ${styleVars.colors.onPrimaryBg};
  }
  a  {
    color: ${styleVars.colors.text};
  }

`;

const AccountWrapper = styled(Wrapper)`
  display: grid;
  align-items: center;
  font-weight: bold;
  z-index: 1;
  @media all and (${styleVars.breakpoints.mediumUp}){
    grid-template-columns: 1fr 25px;
  }
`;

const AccountProfile = styled.p`
  display: none;
  @media all and (${styleVars.breakpoints.mediumUp}){
    display: block;
  }
`;

const AccountIcon = styled.a`
  display: block;
  @media all and (${styleVars.breakpoints.mediumUp}){
    display: none;
  }
`;

class HeaderNotLogged extends Component {
  constructor() {
    super();
    this.modeToLogin = this.modeToLogin.bind(this);
    this.modeToRegister = this.modeToRegister.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  modeToLogin() {
    this.props.modeToLogin()
    this.props.openDrawer()
  }

  modeToRegister() {
    this.props.modeToRegister()
    this.props.openDrawer()
  }

  toggleDrawer(){
    if(this.props.drawerOpened) {
      this.props.closeDrawer()
    } else {
      this.props.openDrawer()
    }

  }

  render() {
    return (
      <AccountNotLogged>
        <AccountWrapper>
          <AccountProfile>
            Welcome,
            <a href="#" onClick={this.modeToLogin}> login </a>
            or
            <a href="#" onClick={this.modeToRegister}> register </a>
            for start!
          </AccountProfile>
          <AccountIcon onClick={this.toggleDrawer}>
            <img src={iconAccount} alt="open login panel"/>
          </AccountIcon>
        </AccountWrapper>
        <HeaderDrawer  opened={this.props.drawerOpened}/>
      </AccountNotLogged>
    )
  }
}

export default connect(mapStateToProps, mapDispatcheToProps)(HeaderNotLogged)
