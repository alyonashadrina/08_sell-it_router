import React from 'react';
import styled from 'styled-components';
import { Button, Input } from '../styleClasses';
import { styleVars } from '../styleVars';
import { reduxForm, Field } from 'redux-form'

import { connect } from "react-redux";
import { logIn } from '../actions/account';

const mapStateToProps = state => ({ loggedIn: state.account.loggedIn });
const mapDispatcheToProps = dispatch => ({ logIn: () => dispatch(logIn()) });

const LabelText = styled.span`
  position: absolute;
  left: ${styleVars.spaces.spaceSm};
  top: 50%;
  transform: translateY(-50%);
  transition: transform .2s;
`;

const InputField = styled(Input)`
  width: 100%;
  padding-left: ${styleVars.spaces.spaceSm};
  color: ${styleVars.colors.onPrimaryBg};
  &:focus + ${LabelText},
  &:not(:placeholder-shown) + ${LabelText} {
    transform: translateY(120%) scale(.9);
  }
`;

const FieldGroup = styled.label`
  display: block;
  width: 100%;
  border-bottom: 2px solid ${styleVars.colors.onPrimaryBg};
  font-size: ${styleVars.fontSizes.small};
  position: relative;
  margin-bottom: ${styleVars.spaces.spaceLg};
`;

const Submit = styled(Button)`
  margin-top: ${styleVars.spaces.spaceLg};
  text-transform: capitalize;
  width: 100%;
  background: white;
  color: ${styleVars.colors.text}
`;

let FormSignIn = props => (
   <form onSubmit={props.handleSubmit}>
      <FieldGroup>
        <Field name="email" component={InputField} type="email" placeholder=" "/>
        <LabelText htmlFor="email"> Email </ LabelText>
      </FieldGroup>
      <FieldGroup>
        <Field name="password" component={InputField} type="password" placeholder=" "/>
        <LabelText htmlFor="password"> Password </ LabelText>
      </FieldGroup>
      <Submit type="submit" onClick={props.logIn}> Submit </ Submit>
   </form>
)

FormSignIn = reduxForm({
    form: 'FormSignIn',
})(FormSignIn)

// export default FormSignIn
export default connect(mapStateToProps, mapDispatcheToProps)(FormSignIn)
