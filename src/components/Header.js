import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";
import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { Wrapper, Input } from '../styleClasses';

import { connect } from "react-redux";

import HeaderLogged from './HeaderLogged';
import HeaderNotLogged from './HeaderNotLogged';

import logoBlueSm from '../assets/logo-blue--sm.png';
import iconSearch from '../assets/icon-search.svg';

import avatar from '../assets/avatar.png';

const mapStateToProps = state => ({ loggedIn: state.account.loggedIn });

const HeaderBlock = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
  display: grid;
  height: 57px;
  color: ${styleVars.colors.primary};
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-columns: 20% 1fr calc(25px + ${styleVars.spaces.spaceSm} * 2);
  @media all and (${styleVars.breakpoints.mediumUp}){
    grid-template-columns: 195px 1fr ${styleVars.other.sidePanelWidth};
  }
`;

const HeaderLogo = styled(Wrapper)`
  padding: 5px;
  align-self: center;
`;

const HeaderSearch = styled(Wrapper)`
  padding-top: ${styleVars.spaces.spaceSm};
  padding-bottom: ${styleVars.spaces.spaceSm};
  align-self: center;
  form {
    position: relative;
  }
  a {
    position: absolute;
    left: ${styleVars.spaces.spaceXsm} / 2;
    bottom: ${styleVars.spaces.spaceXsm};
  }
`;

const SearchInput = styled(Input)`
  border-bottom: 2px solid ${styleVars.colors.primary};
  width: 100%;
  max-width: 450px;
  padding-left: ${styleVars.spaces.spaceLg};
  font-size: pxToRem(14);
`;

const Header = (props) => (
  <HeaderBlock>
    <HeaderLogo>
      <Link to="/">
        <img src={logoBlueSm} alt="Sell it! - logo"/>
      </Link>
    </HeaderLogo>
    <HeaderSearch>
      <form>
        <Link to="/login">
          <img src={iconSearch} alt="search"/>
        </Link>
        <SearchInput type="text" />
      </form>
    </HeaderSearch>
    { props.loggedIn ? <HeaderLogged avatar={avatar} name="Name Realyrealyrealy Long-Long-Long Name"/> : <HeaderNotLogged HeaderDrawerOpened={props.HeaderDrawerOpened} openDrawer={props.openDrawer} closeDrawer={props.closeDrawer}/> }
  </HeaderBlock>
)
export default connect(mapStateToProps)(Header)
