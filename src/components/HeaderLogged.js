import React from 'react';
import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { Wrapper, Button } from '../styleClasses';

import iconAccount from '../assets/icon-account.svg';

import { connect } from "react-redux";
import { logOut } from '../actions/account';

const mapStateToProps = state => ({ loggedIn: state.account.loggedIn });
const mapDispatcheToProps = dispatch => ({ logOut: () => dispatch(logOut()) });

const AccountDropdown = styled.div`
  display: none;
  grid-template-columns: 1fr 1fr;
  grid-column-start: 1;
  grid-column-end: 3;
  font-weight: normal;
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;

  a:hover:before {
    content: '';
    display: block;
    position: absolute;
    width: 5px;
    height: 100%;
    left: 0;
    top: 0;
    background: ${styleVars.colors.text};
  }
`;
const AccountDropdownBtn = styled(Button)`
  position: relative;
  text-align: center;
  &:hover:before {
    content: '';
    display: block;
    position: absolute;
    width: 5px;
    height: 100%;
    left: 0;
    top: 0;
    background: ${styleVars.colors.text};
  }
`;


const AccountLogged = styled.div`
  padding-top: ${styleVars.spaces.spaceXsm};
  padding-bottom: ${styleVars.spaces.spaceXsm};
  background-color: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  position: relative;
  display: grid;
  align-items: center;
  a {
    color: ${styleVars.colors.onPrimaryBg};
  }
  &:hover ${AccountDropdown} {
    display: grid;
  }
`;

const AccountWrapper = styled(Wrapper)`
    display: grid;
    align-items: center;
    font-weight: bold;
    @media all and (${styleVars.breakpoints.mediumUp}){
      grid-template-columns: 1fr 25px;

    }

`;

const AccountProfile = styled.a`
    display: none;
    @media all and (${styleVars.breakpoints.mediumUp}){
      display: block;
    }
    img {
      border-radius: 100%;
      width: 40px;
      margin-right: ${styleVars.spaces.spaceXsm};
      vertical-align: middle;
    }
    span {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
      max-width: 240px;
      display: inline-block;
      vertical-align: text-top;
    }
`;



const HeaderLogged = (props) => (
  <AccountLogged>
    <AccountWrapper>
      <AccountProfile href="#">
        <img src={props.avatar} alt={props.name + ' avatar'}/>
        <span>{props.name}</span>
      </AccountProfile>
      <a href="#" onClick={props.logOut}>
        <img src={iconAccount} alt="log out"/>
      </a>
    </AccountWrapper>
    <AccountDropdown>
      <AccountDropdownBtn href="#"> Add new post </AccountDropdownBtn>
      <AccountDropdownBtn href="#"> btn Profile </AccountDropdownBtn>
    </AccountDropdown>
  </AccountLogged>
)
// export default HeaderNotLogged;
export default connect(mapStateToProps, mapDispatcheToProps)(HeaderLogged)
