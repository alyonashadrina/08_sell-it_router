import React from 'react';
import styled from 'styled-components';
import { MainContainer } from '../styleClasses';

import background from '../assets/bg-page-login.png';

const LoginContainer = styled(MainContainer)`
  background: url(${background});
  background-size: cover;
`;

const Login = (props) => {
  return(
  <LoginContainer />

)}

export default Login;
