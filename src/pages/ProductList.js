import React, { Component } from 'react';
import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { ContainerLg, MainContainer } from '../styleClasses';

import ProductCard from '../components/ProductCard';

import { connect } from "react-redux";
import { fetchProducts } from '../actions/products';

const mapStateToProps = state => ({ productList: state.products.productList });
const mapDispatcheToProps = dispatch => ({ fetchProducts: () => dispatch(fetchProducts()) });

class ProductList extends Component {

  componentDidMount() {
    this.props.fetchProducts()
  }

  render() {

    let listOfProducts = this.props.productList.map((product, i) => {
      let text = ( product.text ? product.text : 'Item' )
      let image = ( product.images[0] ? product.images[0].file : 'https://pbs.twimg.com/profile_images/507251035929190400/BDUL3Uzt_400x400.png' )
      return <ProductCard img={image} title={text} id={product.pk} key={i}/>
    });

    // console.log('this.props', this.props)

    const ProductContainer = styled(ContainerLg)`
      display: grid;
      grid-column-gap: ${styleVars.spaces.spaceMd};
      grid-row-gap: ${styleVars.spaces.spaceMd};
      @media all and (${styleVars.breakpoints.smallUp}) {
        grid-template-columns: repeat(2, 1fr)
      }
      @media all and (${styleVars.breakpoints.mediumUp}) {
        grid-template-columns: repeat(4, 1fr)
      }
    `;

    return (
      <MainContainer>
        <ProductContainer>
          {listOfProducts}
        </ProductContainer>
      </MainContainer>
    )
  }

};


export default connect(mapStateToProps, mapDispatcheToProps)(ProductList)
