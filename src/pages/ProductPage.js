import React, { Component } from 'react';
import styled from 'styled-components';
import { styleVars, pxToRem } from '../styleVars';
import { ContainerMd, MainContainer } from '../styleClasses';

import { connect } from "react-redux";
import { fetchSingleProduct } from '../actions/products';

const mapStateToProps = state => ({ singleProduct: state.products.singleProduct });
const mapDispatcheToProps = dispatch => ({ fetchSingleProduct: (id) => dispatch(fetchSingleProduct(id)) });


const ProductContainer = styled(ContainerMd)`
    @media all and (${styleVars.breakpoints.mediumUp}){
      display: grid;
      grid-column-gap: ${styleVars.spaces.spaceSm};
      grid-template-columns: repeat(2, 1fr)
    }
`;

const Info = styled.div`
`;

const Title = styled.div`
  h1 {
    font-size: ${pxToRem(32)};
    font-family: "Myriad Pro";
    color: rgb(0, 0, 0);
    padding-bottom: ${styleVars.spaces.spaceLg};
  }
`;

const SellInfo = styled.div`
  font-weight: bold;
  padding-bottom: ${styleVars.spaces.spaceLg};
`;

const Seller = styled.span`
  padding-right: ${styleVars.spaces.spaceMd};
  a {
    color: ${styleVars.colors.primary}
  }
`;

const Price = styled.span`
  color: ${styleVars.colors.selected};
  font-size: ${pxToRem(32)};
  display: inline-block;
  &:after {
    content: ' $'
  }
`;


class ProductPage extends Component {

  componentDidMount() {
    this.props.fetchSingleProduct(this.props.id)
  }

  render() {
    const product = this.props.singleProduct;

    let images = product.images;
    let image = 'https://pbs.twimg.com/profile_images/507251035929190400/BDUL3Uzt_400x400.png';
    console.log('images', images === true)
    if (images && images.length > 0) {
      image = images[0].file
    }

    let owner = product.owner;
    let ownerName = '';
    let userName = '';

    if (owner) {
     ownerName = owner.first_name + ' ' + owner.last_name;
     userName = owner.username;
    }

    let title = product.text;
    let price = product.price;
    let isContractPrice = product.contract_price;

    return(
      <MainContainer>
        <ProductContainer>
          <img src={image} alt={title}/>
          <Info>
            <Title>
              <h1>{title}</h1>
              { isContractPrice ? <p>contract price</p> : ''}
            </Title>
            <SellInfo>
              <Seller>
              from<a href={`/users/${userName}`}> {ownerName}</a>
              </Seller>
              <Price>{price}</Price>
            </SellInfo>
            <p>
              Some description text from WYSIWYG editer

                Can be BOLD Italic

                Can have  list:
                	- first
                	- second

                And anything from WYSIWYG editor

                Some line
                Some line
                Some  line

                Finished on this line.
            </p>
          </Info>
        </ProductContainer>
      </MainContainer>
    )
  }
}

// export default ProductPage;
export default connect(mapStateToProps, mapDispatcheToProps)(ProductPage)
